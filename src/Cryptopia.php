<?php
declare(strict_types=1);

namespace TradingDev\Cryptopia;

require_once '../vendor/autoload.php';

use BrianFaust\Http\Http;
use BrianFaust\Http\PendingHttpRequest;

class Cryptopia
{
    private $key;
    private $secret;

    public function __construct(string $key, string $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }

    public function getCurrencies(): ?array
    {
        return $this->sendPublicRequest('GetCurrencies');
    }

    public function getTradePairs(): ?array
    {
        return $this->sendPublicRequest('GetTradePairs');
    }

    public function getMarkets(?string $baseMarket = null, ?int $hours = null): ?array
    {
        $path = Cryptopia::addOptionalParametersToPath('GetMarkets', $baseMarket, $hours);
        return $this->sendPublicRequest($path);
    }

    public function getMarket($market, ?int $hours): ?array
    {
        $path = Cryptopia::addOptionalParametersToPath('GetMarket', $market, $hours);
        return $this->sendPublicRequest($path);
    }

    public function getMarketHistory($market, ?int $hours): ?array
    {
        $path = Cryptopia::addOptionalParametersToPath('GetMarketHistory', $market, $hours);
        return $this->sendPublicRequest($path);
    }

    public function getMarketOrders($market, ?int $orderCount): ?array
    {
        $path = Cryptopia::addOptionalParametersToPath('GetMarketOrders', $market, $orderCount);
        return $this->sendPublicRequest($path);
    }

    private static function addOptionalParametersToPath(?string $path, ...$parameters): ?string
    {
        foreach ($parameters as $parameter) {
            if ($parameter != null) {
                $path = $path . '/' . $parameter;
            }
        }
        return $path;
    }

    private function sendPublicRequest(string $path): ?array
    {
        $url = "https://www.cryptopia.co.nz/api/{$path}";
        $response = PendingHttpRequest::new()->get($url);
        $json = $response->json();
        if ($json) {
            return $json;
        }
        printf("Invalid response for path %s\n", $path);
        printf("HTTP Status code: %d\n", $response->getStatusCode());
        printf("HTTP Body: %s\n", $response->getBody());
        return null;
    }

    public function getBalance(?string $currency = null, ?int $currencyId = null): ?array
    {
        $arguments = [
            'Currency' => $currency,
            'CurrencyId' => $currencyId,
        ];
        return $this->sendPrivateRequest('GetBalance', $arguments);
    }

    public function getDepositAddress(?string $currency = null, ?int $currencyId = null): ?array
    {
        $arguments = [
            'Currency' => $currency,
            'CurrencyId' => $currencyId,
        ];
        return $this->sendPrivateRequest('GetDepositAddress', $arguments);
    }

    public function getOpenOrders(?string $market = null, ?int $tradePairId = null, ?int $count = null): ?array
    {
        $arguments = [
            'Market' => $market,
            'TradePairId' => $tradePairId,
            'Count' => $count,
        ];
        return $this->sendPrivateRequest('GetOpenOrders', $arguments);
    }

    public function getTradeHistory(?string $market = null, ?int $tradePairId = null, ?int $count = null): ?array
    {
        $arguments = [
            'Market' => $market,
            'TradePairId' => $tradePairId,
            'Count' => $count,
        ];
        return $this->sendPrivateRequest('GetTradeHistory', $arguments);
    }

    public function getDepositTransactions(?int $count = null): ?array
    {
        $arguments = [
            'Type' => 'Deposit',
            'Count' => $count,
        ];
        return $this->sendPrivateRequest('GetTransactions', $arguments);
    }

    public function getWithdrawTransactions(?int $count = null): ?array
    {
        $arguments = [
            'Type' => 'Withdraw',
            'Count' => $count,
        ];
        return $this->sendPrivateRequest('GetTransactions', $arguments);
    }

    public function submitBuyOrder(?string $market, ?int $tradePairId, float $rate, float $amount): ?array
    {
        $arguments = [
            'Market' => $market,
            'TradePairId' => $tradePairId,
            'Type' => 'Buy',
            'Rate' => $rate,
            'Amount' => $amount,
        ];
        return $this->sendPrivateRequest('SubmitTrade', $arguments);
    }

    public function submitSellOrder(?string $market, ?int $tradePairId, float $rate, float $amount): ?array
    {
        $arguments = [
            'Market' => $market,
            'TradePairId' => $tradePairId,
            'Type' => 'Sell',
            'Rate' => $rate,
            'Amount' => $amount,
        ];
        return $this->sendPrivateRequest('SubmitTrade', $arguments);
    }

    public function cancelAllOrders(): ?array
    {
        $arguments = [
            'Type' => 'All',
        ];
        return $this->sendPrivateRequest('CancelTrade', $arguments);
    }

    public function cancelOrder(string $orderId): ?array
    {
        $arguments = [
            'Type' => 'Trade',
            'OrderId' => $orderId,
        ];
        return $this->sendPrivateRequest('CancelTrade', $arguments);
    }

    public function cancelOrdersForTradePair(int $tradePairId): ?array
    {
        $arguments = [
            'Type' => 'TradePair',
            'OrderId' => $tradePairId,
        ];
        return $this->sendPrivateRequest('CancelTrade', $arguments);
    }

    public function submitTip(?string $currency, ?int $currencyId, int $activeUsers, float $amount): ?array
    {
        $arguments = [
            'Currency' => $currency,
            'CurrencyId' => $currencyId,
            'ActiveUsers' => $activeUsers,
            'Amount' => $amount,
        ];
        return $this->sendPrivateRequest('SubmitTip', $arguments);
    }

    public function submitWithdraw(?string $currency, ?int $currencyId, string $address, ?string $paymentId, float $amount): ?array
    {
        $arguments = [
            'Currency' => $currency,
            'CurrencyId' => $currencyId,
            'Address' => $address,
            'PaymentId' => $paymentId,
            'Amount' => $amount,
        ];
        return $this->sendPrivateRequest('SubmitWithdraw', $arguments);
    }

    public function submitTransfer(?string $currency, ?int $currencyId, string $username, float $amount): ?array
    {
        $arguments = [
            'Currency' => $currency,
            'CurrencyId' => $currencyId,
            'Username' => $username,
            'Amount' => $amount,
        ];
        return $this->sendPrivateRequest('SubmitTransfer', $arguments);
    }

    private function sendPrivateRequest(string $method, array $arguments): ?array
    {
        $url = "https://www.cryptopia.co.nz/api/{$method}";
        $nonce = uniqid('', true);

        $normalizedArguments = array_filter($arguments);
        $postData = json_encode($normalizedArguments);
        $hash = md5($postData, true);
        $requestContentBase64String = base64_encode($hash);

        $signature = $this->key . "POST" . strtolower(urlencode($url)) . $nonce . $requestContentBase64String;
        $hmacSignature = base64_encode(hash_hmac("sha256", $signature, base64_decode($this->secret), true));
        $authorizationValue = 'amx ' . $this->key . ":" . $hmacSignature . ":" . $nonce;
        $headers = [
            'Authorization' => $authorizationValue
        ];

        $request = Http::withHeaders($headers)
          ->asJson();

        $response = Http::withHeaders($headers)
            ->asJson()
            ->post($url, $normalizedArguments);

        $json = $response->json();
        if ($json) {
            return $json;
        }
        printf("Invalid response for method %s\n", $method);
        printf("HTTP Status code: %d\n", $response->getStatusCode());
        printf("HTTP Body: %s\n", $response->getBody());
        return null;
    }
}
